#mysqlreporter
##Description
A reporter for mocha that stores results directly to a mysql db

##Usage
```sh
$ npm install mysqlreporter
...
$ MYSQLURL=mysqldb://dbuser:dbpassword@dbhost/dbname mocha -R mysqlreporter test/one.js --no-exit
```
programmatically:
```sh
var mocha = new Mocha({
    ui: 'bdd',
    reporter: "mysqlreporter",
    reporterOption: {
    	url: "mysqldb://dbuser:dbpassword@dbhost/dbname"
	}
});
mocha.addFile("test/one.js");
mocha.run(...);
```

I like to use this with mocha-multi reporter, using spec to print test results, something like this
```sh
$ npm install --save-dev mocha-multi
...
$ npm install --save-dev mysqlreporter
...
$ MYSQLURL="mysqldb://tester:changeme@localhost/testruns" multi="spec=- mysqlreporter=/dev/null" mocha -R mocha-multi --no-exit
```
