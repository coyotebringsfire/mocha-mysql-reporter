var debug = require('debug')('mocha:mysqlreporter');
var mysql = require('mysql');
var dateFormat = require('dateFormat');
var now;
var snap = new (require('env-snapshot').EnvSnapshot)();
var flatten = require('flat');
var util = require('util');

module.exports = MochaMysqlReporter;

/**
 * mocha reporter that stores results in mysql db
 * @class
 * @param {MochaRunner} runner - mocha runner object
 * @param {object} options - mocha runner options
 */
function MochaMysqlReporter(runner, options) {
  var db = null;

  var passes = [];
  var failures = [];
  var database;

  var meta = {
    user: snap.process_env.USER,
    host: snap.os_hostname,
    type: snap.os_type,
    platform: snap.os_platform,
    arch: snap.os_arch,
    release: snap.os_release,
    totalmem: snap.os_totalmem,
    freemem: snap.os_freemem
  };

  var mysqlUrl = (options && options.url) || process.env.MYSQLURL;

  var runnerEnd = new Promise(function(resolve) {
    runner.on('end', function() {
      debug('runner.end');
      resolve('runner ended');
    });
  });
  var mysqlConnect = new Promise(function(resolve) {
    var mysqlURLmatch;
    var mysqlUrlRegex = /mysqldb:\/\/([^:]+):([^@]+)@([^\/]+)\/(.+)/;
    debug('connecting to %j', mysqlUrl);
    mysqlURLmatch = mysqlUrl.match(mysqlUrlRegex);
    database = mysqlURLmatch[4];
    var connection = mysql.createConnection({
      host: mysqlURLmatch[3],
      user: mysqlURLmatch[1],
      password: mysqlURLmatch[2],
      database: mysqlURLmatch[4]
    });

    connection.connect();

    db = connection;
    resolve({});
  });

  if (!(this instanceof MochaMysqlReporter)) {
    return new MochaMysqlReporter(runner);
  }

  runner.on('pass', function(test) {
    now = new Date();
    meta.timestamp = dateFormat(now, 'isoDateTime', true);
    passes.push({
      suite: test.fullTitle().match(new RegExp('(.*) ' + test.title))[1],
      test: test.title,
      duration: test.duration,
      pass: true,
      meta: JSON.stringify(meta)
    });
  });

  runner.on('fail', function(test, err) {
    now = new Date();
    meta.timestamp = dateFormat(now, 'isoDateTime', true);
    failures.push({
      suite: test.fullTitle().match(new RegExp('(.*) ' + test.title))[1],
      test: test.title,
      duration: test.duration,
      pass: false,
      err: err.message,
      meta: JSON.stringify(meta)
    });
  });

  runnerEnd.then(function() {
    debug('runnerEnd');
    mysqlConnect.then(function onDBConnect() {
      updateDB();
    }, function onDBConnectFail() {
      debug('failure to connected to mysql');
    });
  }, function onRejectedPromise(err) {
    debug('error running tests: %s', err.message);
  });

  /**
   * execute sql query to update db
   */
  function updateDB() {
    var allDeferreds = [];
    debug('updating db');
    passes.concat(failures).forEach(function saveTestResults(test) {
      var _test = test;
      var deferred = new Promise(function(resolve, reject) {
        debug('testrun to insert: %s', JSON.stringify(_test));
        if (options.meta === false) {
          _test.meta = undefined;
        }
        _test = flatten(_test);
        db.query(util.format('INSERT INTO %s (%s) VALUES (%s)', database,
            getKeys(_test), getValues(_test)), function(err, rows, fields) {
          if (err) {
            return reject(err);
          }
          resolve({rows, fields});
        });
      });
      allDeferreds.push(deferred);
    });
    Promise.all(allDeferreds).then(function allDBUpdatesDone() {
      db.end();
      process.exit(failures.length);
    }, function onDBErr(err) {
      debug('error saving to mysql: %s', err.message);
      db.end();
      process.exit(failures.length);
    });
  }

  /**
   * return a square bracketed string representation of the object keys
   * @param {object} object - the object to get keys
   * @return {string} - eg {'name': 'test', 'age': 39 } => "['name', 'age']"
   */
  function getKeys(object) {
    return JSON.stringify(object).replace('[', '').replace(']', '');
  }

  /**
   * return an array of non inherited values for the given object, analogous to Object.keys()
   * @param {object} object - the object to get values
   * @return {string} - eg {'name': 'test', 'age': 39 } => "['test', 39]"
   */
  function getValues(object) {
    var v;
    var values = [];
    for (v in Object.keys(object)) {
      if (object.hasOwnProperty(v)) {
        values.push(Object.keys(object)[v]);
      }
    }
    return JSON.stringify(object).replace('[', '').replace(']', '');
  }
}
